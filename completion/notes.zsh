#compdef notes

_notes_complete_commands() {
    local -a subcommands
    subcommands=(
        "help:get help"
        "open:open a note"
        "list:list current links that can be followed"
        "follow:follow a link"
        "get:get file path"
    )
    _describe -t commands 'notes' subcommands
}

_notes_complete_paths() {
    local prefix=$HOME/Notes/
    local directories=$prefix$cur
    _values 'paths' ${$(find -L "$prefix" -type f 2>/dev/null | sed "s#${prefix}##" ):-""}
}

_notes() {
    local culr="${words[CURRENT]}"
    if (( CURRENT <= 2)); then
        _notes_complete_commands
    else
        case "${words[2]}" in
            open|o|get|g)
                _notes_complete_paths
                ;;
        esac
    fi
    _arguments "--version[Output version information]" "-V[Output version information]" "--help[Output help message]" "-h[Output help message]"

}

_notes
