_notes_complete_commands() 
{
    local commands="open o follow f list l get g help"
    COMPREPLY+=($(compgen -W "${commands}" -- "${cur}"))
}

_notes_complete_paths()
{
    local prefix="$HOME/Notes/"
    local files=($(compgen -f "$prefix$cur"))
    local i=0 file
    for file in "${files[@]}"; do
        [[ -d $file ]] && file="$file/"
        COMPREPLY+=("${file#$prefix}")
        let i+=1
    done
    if [[ $i -eq 1 && -d $file ]]; then
        compopt -o nospace
    fi
}

_notes()
{
    COMPREPLY=()
    local cur="${COMP_WORDS[COMP_CWORD]}"
    if [[ $COMP_CWORD -le 1 ]]; then
        _notes_complete_commands
    else
        case "${COMP_WORDS[1]}" in
            open|o|get|g)
                _notes_complete_paths
                ;;
        esac
    fi
    COMPREPLY+=($(compgen -W "--help -h --version -V" -- ${cur}))
}

complete -o filenames -F _notes notes
