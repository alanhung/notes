use clap::Parser;
use open as xdg_open;
use std::fs;
use std::{
    ffi::OsStr,
    io::{BufRead, Write},
    os::unix::prelude::OsStrExt,
    path::{Path, PathBuf},
};
use url::{self, Url};
// hi
#[derive(Debug, clap::Parser)]
#[command(version, author, about)]
struct Args {
    #[command(subcommand)]
    command: Cmds,
}

#[derive(Debug, clap::Subcommand)]
enum Cmds {
    #[command(alias = "o")]
    Open { path: String },
    #[command(alias = "f")]
    Follow { number: u64 },
    #[command(alias = "l")]
    List,
    #[command(alias = "g")]
    Get { path: String },
}

fn open_markdown(path: &Path) -> anyhow::Result<(String, Vec<String>)> {
    let contents = fs::read_to_string(path)?;

    let mut links = Vec::new();
    let mut in_link = false;

    let mut events = Vec::new();
    let mut p = pulldown_cmark::Parser::new(&contents).peekable();

    while let Some(event) = p.next() {
        events.push(match event {
            pulldown_cmark::Event::Start(pulldown_cmark::Tag::Link(link_type, url_str, title)) => {
                in_link = true;
                let mut url_str = url_str.to_string();
                let parsed_url = match url::Url::parse(&url_str) {
                    Err(url::ParseError::RelativeUrlWithoutBase) => {
                        while url_str.starts_with("../") {
                            url_str = url_str[3..].to_string();
                        }
                        if url_str.starts_with("./") {
                            url_str = url_str[2..].to_string();
                        }
                        let mut final_str = String::from("file://");
                        final_str.push_str(&String::from_utf8_lossy(
                            path.parent().unwrap().as_os_str().as_bytes(),
                        ));
                        if !final_str.ends_with('/') {
                            final_str.push('/');
                        }
                        final_str.push_str(&url_str);
                        final_str
                    }
                    _ => url_str.to_string(),
                };
                links.push(parsed_url.clone());
                pulldown_cmark::Event::Start(pulldown_cmark::Tag::Link(
                    link_type,
                    parsed_url.into(),
                    title,
                ))
            }
            pulldown_cmark::Event::Text(ref text) => {
                if in_link {
                    if let Some(pulldown_cmark::Event::End(pulldown_cmark::Tag::Link(..))) =
                        p.peek()
                    {
                        pulldown_cmark::Event::Text(format!("{}[{}]", text, links.len() - 1).into())
                    } else {
                        event
                    }
                } else {
                    event
                }
            }
            pulldown_cmark::Event::End(pulldown_cmark::Tag::Link(..)) => {
                in_link = false;
                event
            }
            _ => event,
        });
    }

    let settings = mdcat::Settings {
        resource_access: mdcat::ResourceAccess::LocalOnly,
        syntax_set: syntect::parsing::SyntaxSet::default(),
        terminal_capabilities: mdcat::TerminalCapabilities::detect(),
        terminal_size: mdcat::TerminalSize::default(),
    };
    let env = mdcat::Environment::for_local_directory(&path.parent().unwrap())?;
    let mut sink = Vec::new();
    mdcat::push_tty(&settings, &env, &mut sink, events.into_iter())?;

    Ok((String::from_utf8_lossy(&sink).to_string(), links))
}

fn open_local(path: &Path) -> anyhow::Result<(String, Vec<String>)> {
    match path.extension().and_then(OsStr::to_str) {
        Some("md") => open_markdown(path),
        _ => {
            xdg_open::that(path)?;
            Ok((String::new(), Vec::new()))
        }
    }
}

fn open_url(url: Url) -> anyhow::Result<(String, Vec<String>)> {
    match url.scheme() {
        "file" => open_local(&PathBuf::from(url.path())),
        _ => {
            xdg_open::that(url.as_str())?;
            Ok((String::new(), Vec::new()))
        }
    }
}

fn open(path: &str) -> anyhow::Result<()> {
    let Some(mut note_path) = dirs::home_dir() else {
        return Err(anyhow::anyhow!("Cannot find home directory"));
    };
    note_path.push("Notes");
    note_path.push(path);
    if note_path.is_dir() {
        note_path.push("index.md");
    }
    let (content, links) = open_local(&note_path)?;
    handle_open(&content, &links)
}

fn follow(number: u64) -> anyhow::Result<()> {
    let Some(mut data_prefix) = dirs::data_local_dir() else {
        return Err(anyhow::anyhow!("Cannot find data dir"));
    };
    data_prefix.push("notes");
    data_prefix.push("urls");
    let links = fs::read_to_string(data_prefix)?;
    let links = links.split('\n').collect::<Vec<_>>();
    let Some(url) = links.get(number as usize) else {
        return Err(anyhow::anyhow!("bad number"));
    };
    let Ok(url) = Url::parse(url) else {
        return Err(anyhow::anyhow!("bad url"));
    };
    let (content, links) = open_url(url)?;
    handle_open(&content, &links)
}

fn handle_open(content: &str, links: &[String]) -> anyhow::Result<()> {
    if !content.is_empty() {
        let mut stdout = std::io::stdout().lock();
        stdout.write_all(content.as_bytes())?;
    }
    if !links.is_empty() {
        write_db(links)?;
    }
    Ok(())
}

fn write_db(links: &[String]) -> anyhow::Result<()> {
    let Some(mut data_path) = dirs::data_local_dir() else {
        return Err(anyhow::anyhow!("Cannot find data dir"));
    };
    data_path.push("notes");
    data_path.push("urls");
    std::fs::create_dir_all(data_path.parent().unwrap())?;
    let mut urls_file = std::fs::File::create(data_path)?;
    urls_file.write_all(links.join("\n").as_bytes())?;
    Ok(())
}

fn main() -> anyhow::Result<()> {
    let args = Args::parse();
    match args.command {
        Cmds::Open { path } => open(&path),
        Cmds::Follow { number } => follow(number),
        Cmds::List => {
            let Some(mut data_path) = dirs::data_local_dir() else {
                return Err(anyhow::anyhow!("Cannot find data dir"));
            };
            data_path.push("notes");
            data_path.push("urls");
            let lines = std::io::BufReader::new(std::fs::File::open(&data_path)?).lines();
            for (i, line) in lines.enumerate() {
                println!("[{}] {}", i, line?);
            }
            Ok(())
        }
        Cmds::Get { path } => {
            let Some(mut note_path) = dirs::home_dir() else {
                return Err(anyhow::anyhow!("Cannot find home directory"));
            };
            note_path.push("Notes");
            note_path.push(path);
            println!("{}", note_path.to_str().unwrap());
            Ok(())
        }
    }
}
